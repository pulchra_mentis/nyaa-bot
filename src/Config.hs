module Config where

import BasicPrelude
import qualified Data.Map.Strict as DMS
import Discord.Types (GuildId, ChannelId, RoleId, UserId)

-- Emotes that are interpreted as approval to join the chat
yesEmotes :: [Text]
yesEmotes = [
    "thumbsup", "\128077",
    "call_me", "\129305",
    "yes"
  ]

-- Mapping of text channels to voice
voiceChannelMapping :: Map ChannelId ChannelId
voiceChannelMapping = DMS.fromList [
      -- Queer Emotes: #bot-testing-room -> #date-night
      (643006848600899594, 645225361168400384),
      -- LesBDSM: #sfw-vc-text -> #SFW-VC
      (789909285525258251, 600506226002100275),
      -- LesBDSM: #nsfw-vc-text -> #nsfw-vc
      (558708912023666688, 558696081400004612),
      -- LesBDSM: #playtime-vc-text-channel -> #playtime fun (nsfw)
      (807620533654913025, 807620454323716128)
  ]

-- Don't automatically lock these channels when someone enters
noAutoLockChannels :: [ChannelId]
noAutoLockChannels = [600506226002100275] -- #SFW-VC

-- Mapping of voice channels to text
textChannelMapping :: Map ChannelId ChannelId
textChannelMapping = DMS.fromList . map swap . DMS.toList $ voiceChannelMapping

lesbdsmServerId :: GuildId
lesbdsmServerId = 558696081395810315

waitingRoomChannel :: ChannelId
waitingRoomChannel = 589236594226102293

userLogChannel :: ChannelId
userLogChannel = 810193609818243102

dynoLogChannel :: ChannelId
dynoLogChannel = 571796639594053652

sleepyChannel :: ChannelId
sleepyChannel = 812557695307218944

-- The list of roles to toggle the connect permission on
rolesToToggle :: [RoleId]
rolesToToggle = [
    558701595194556417, -- Dominant
    558701600210681857, -- Switch
    687314755295510556, -- Kink-Curious
    558701598952652821  -- Submissive
  ]

modRole :: RoleId
modRole = 558699116142788638

reneeUID :: UserId
reneeUID = 330884524030558209

myceUID :: UserId
myceUID = 270866281790570496

kawaUID :: UserId
kawaUID = 167100249272221698

jennyUID :: UserId
jennyUID = 200395680496353280

blakeUID :: UserId
blakeUID = 464796266283663360

skyeUID :: UserId
skyeUID = 221613748408942592

kathyUID :: UserId
kathyUID = 160083432548990976
