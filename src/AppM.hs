{-# LANGUAGE TypeSynonymInstances, FlexibleContexts, FlexibleInstances, TypeFamilies #-}

module AppM where

import BasicPrelude
import Control.Monad.Reader (MonadReader, ReaderT, ask, runReaderT)
import Control.Monad.Trans.Maybe (MaybeT(..))
import Data.Acid (AcidState, EventResult, EventState, QueryEvent, UpdateEvent, query, update)
import Discord
import Katip

import AcidDefinitions


data AppContext = AppContext {
  appDiscord :: DiscordHandle,
  appAcid :: AcidState AppDB
}


-- MonadDiscord class
class Monad m => MonadDiscord m where
  liftDiscord :: DiscordHandler a -> m a

instance MonadDiscord DiscordHandler where
  liftDiscord = id

instance MonadDiscord m => MonadDiscord (MaybeT m) where
  liftDiscord = lift . liftDiscord


-- App monad stack
newtype AppM a = AppM (KatipContextT (ReaderT AppContext IO) a)
    deriving (Functor, Applicative, Monad, MonadIO, MonadReader AppContext, Katip, KatipContext)

instance MonadDiscord AppM where
  liftDiscord f = do
    AppContext{..} <- ask
    liftIO $ runReaderT f appDiscord

-- We run the app stack inside the DiscordHandler cause it gets invoked from the event handling loop
runAppM :: LogEnv -> AcidState AppDB -> AppM a -> DiscordHandler a
runAppM logEnv acid (AppM f) = do
  ctx <- AppContext <$> ask <*> pure acid
  liftIO
    . flip runReaderT ctx
    $ runKatipContextT logEnv () "main" f


-- Helper methods for running ACID operations in the monad
-- Note that the equational constraint is needed to get this to typecheck, and requires the TypeFamilies extension
appQuery :: (QueryEvent event, MonadReader AppContext m, MonadIO m, EventState event ~ AppDB)
         => event -> m (EventResult event)
appQuery ev = do
  acid <- appAcid <$> ask
  liftIO $ query acid ev

appUpdate :: (UpdateEvent event, MonadReader AppContext m, MonadIO m, EventState event ~ AppDB)
          => event -> m (EventResult event)
appUpdate ev = do
  acid <- appAcid <$> ask
  liftIO $ update acid ev

