-- Aeson type for the base64 encoded state in https://dragory.github.io/role-preview-for-discord/

module RolePreviewTypes (rolePreviewUrl) where

import BasicPrelude
import Data.Aeson
import qualified Data.ByteString.Lazy.Base64 as B64
import Data.Char (intToDigit)
import qualified Data.Text.Lazy as TL
import Discord.Internal.Types (Role(..))
import Numeric (showIntAtBase)

{- Example JSON
  {
    "roles": [
      {
        "id": 1,
        "name": "Example 1",
        "color": "#7d1818"
      },
      ...
    ],
    "colorBlindModes": [
      "Deuteranomaly"
    ]
  }
-}


-- Generates a URL for viewing the accessibility info on all the roles' colours
rolePreviewUrl :: [Role] -> Text
rolePreviewUrl roles = url where
  state = object [
      "colorBlindModes" .= toJSON ["Deuteranomaly" :: Text],
      "roles"           .= toJSON (map jsonifyRole roles)
    ]

  jsonifyRole Role{..} = object [
      "id"    .= toJSON roleId,
      "name"  .= toJSON roleName,
      "color" .= toJSON ("#" <> hexEncode roleColor)
    ]

  stateBase64 = TL.toStrict
              . B64.encodeBase64
              $ encode state
  url = "https://dragory.github.io/role-preview-for-discord/#?state=" <> stateBase64

hexEncode :: Integer -> String
hexEncode x = showIntAtBase 16 intToDigit x ""
