{-# LANGUAGE FlexibleContexts #-}

module Main where

import BasicPrelude
import Control.Monad.Reader (MonadReader)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Acid (openLocalStateFrom, closeAcidState)
import Data.Aeson (encode, eitherDecode)
import Data.Bits
import qualified Data.Map.Strict as DMS
import qualified Data.Text as T
import Safe (headMay)
import System.Environment (getEnv)
import System.IO (stderr)
import UnliftIO.Exception (bracket)

import Discord
import qualified Discord.Requests as R
import Discord.Types as T
import Katip

import AcidDefinitions
import AppM
import Config
import DiscordExtras
import Resources
import RolePreviewTypes


main :: IO ()
main = do
    let mkLogEnv = do
            fileScribe <- mkFileScribe "/opt/nyaa-bot/nyaa-bot.log" (permitItem DebugS) V2
            stdErrScribe <- mkHandleScribe ColorIfTerminal stderr (permitItem DebugS) V2

            registerScribe "file" fileScribe defaultScribeSettings
                =<< registerScribe "stderr" stdErrScribe defaultScribeSettings
                =<< initLogEnv "nyaa-bot" "production"

    bracket mkLogEnv closeScribes $ \logEnv ->
      bracket (openLocalStateFrom "/opt/nyaa-bot" def) closeAcidState $ \acid -> do
        let withLogging = runKatipContextT logEnv () "main"
        let runAppM' = runAppM logEnv acid

        botToken <- T.pack <$> getEnv "BOT_TOKEN"

        userFacingError <- runDiscord $ def { discordToken = botToken,
                                              discordOnStart = runAppM' $ $(logTM) InfoS "Started",
                                              discordOnEnd = withLogging $ $(logTM) InfoS "Terminating...",
                                              discordOnEvent = runAppM' . eventHandler }
        withLogging $ $(logTM) WarningS (logStr userFacingError)

-- Main event processing loop
eventHandler :: Event -> AppM ()
-- Log enabled servers
eventHandler (GuildCreate guild _) = do
    $(logTM) InfoS $ concat [
            "Guild \"",
            logStr $ guildName guild,
            "\" (",
            showLS (guildId guild),
            ") connected"
        ]

-- Check for reacts approving access
-- Using MaybeT to enable short-circuiting exits via fail
eventHandler (MessageReactionAdd ReactionInfo{..}) = void . runMaybeT $ do
    -- Ignore if wrong channel
    voiceChannel <- hoistMaybe $ DMS.lookup reactionChannelId voiceChannelMapping

    -- Only proceed if it's an emote we care about
    unless ((emojiName reactionEmoji) `elem` yesEmotes) $ fail ""

    $(logTM) DebugS $ concat [
        "Handling react ",
        showLS $ emojiName reactionEmoji,
        " in channel ",
        showLS reactionChannelId,
        " by user ",
        showLS reactionUserId,
        " on message ",
        showLS reactionMessageId
      ]

    -- Get message so we know who wrote it
    msg <- liftDiscordWithErrorHandling "Error querying message"
            . restCall
            $ R.GetChannelMessage (reactionChannelId, reactionMessageId)

    let approver = reactionUserId
        candidate = userId $ messageAuthor msg
        textChannel = messageChannel msg

    processAllowUserRequest voiceChannel textChannel approver candidate

-- Voice events (currently not parsed by upstream, so we roll our own parsing)
eventHandler (UnknownEvent "VOICE_STATE_UPDATE" rawObject) = void . runMaybeT $ do
    vcInfo :: VoiceStateInfo
        <- logEither "Decoding event object" . eitherDecode $ encode rawObject

    $(logTM) DebugS $ concat [
        "User ",
        showLS $ voiceUserId vcInfo,
        " is ",
        case voiceChannelId vcInfo of
             Just cid -> "now in " <> showLS cid
             Nothing  -> "has disconnected from voice"
      ]

    -- Update state tracking
    oldChannel <- appQuery $ GetUserChannel (voiceUserId vcInfo)

    case voiceChannelId vcInfo of
         Just cid -> appUpdate $ UpdateUserChannel (voiceUserId vcInfo) cid
         Nothing  -> appUpdate $ RemoveUserChannel (voiceUserId vcInfo)

    oldChannelCount <- channelMemberCount oldChannel
    newChannelCount <- channelMemberCount (voiceChannelId vcInfo)

    -- User left old channel and it's now empty, so reset permissions
    when (isJust oldChannel && oldChannelCount == 0) $ do
      let Just oldChannel' = oldChannel
      txtChannel <- hoistMaybe $ DMS.lookup oldChannel' textChannelMapping
      unlockVC txtChannel oldChannel' "Everyone has left voice - channel is now unlocked."

    -- This is the first person to enter the channel - need to lock
    when (newChannelCount == 1) $ do
      let Just newChannel = voiceChannelId vcInfo
      txtChannel <- hoistMaybe $ DMS.lookup newChannel textChannelMapping

      unless (newChannel `elem` noAutoLockChannels)
        . lockVC txtChannel newChannel
        $  "This voice channel now requires consent to join. To grant consent, please react to the request in this channel with :thumbsup: or use `@nyaa-bot allow USER`.\n"
        <> "You can disable this with `@nyaa-bot unlock`."

-- Message events - for parsing commands
eventHandler (MessageCreate msg@Message{..}) = void . runMaybeT $ do
    let txt = T.toLower messageText
        txtContainsAnyOf = any (flip T.isInfixOf $ txt)

    -- Tell the mods when someone is done reading the rules
    when (T.isInfixOf "rules" txt && messageChannel == waitingRoomChannel && not (userIsBot messageAuthor)) $ do
        liftDiscordWithErrorHandling_ "Rules read notification"
          . restCall
          . R.CreateMessage userLogChannel
          $ concat [
              "@here - ",
              mentionUser (userId messageAuthor),
              " in ",
              mentionChannel waitingRoomChannel,
              " has read the rules"
            ]
        fail ""

    when (T.isInfixOf "can i join" txt && not (userIsBot messageAuthor)) $ do
      voiceChannel <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
      isLocked <- isVCLocked voiceChannel
      unless isLocked
        . liftDiscordWithErrorHandling_ "Rules read notification"
        . restCall
        . R.CreateMessage messageChannel
        $ concat [
              mentionUser (userId messageAuthor),
              " the channel is unlocked - come on in ^_^"
            ]
      fail ""

    -- Ignore if it doesn't mention us (by user ID or role)
    when (null messageMentions && null messageMentionRoles) $ fail ""

    discordCache <- liftDiscord readCache
    guildId <- hoistMaybe messageGuild

    let botUser = userId $ _currentUser discordCache
    let botRole :: RoleId
          = maybe 0 head
          . map (map roleId
                . filter ((==) "nyaa-bot" . roleName)
                . guildRoles
                . fst
                )
          $ (DMS.lookup guildId $ _guilds discordCache)

    unless (botUser `elem` map userId messageMentions || botRole `elem` messageMentionRoles)
      $ fail ""

    -- Parse the command - the first word will typically be <@!820340010321838150>
    $(logTM) DebugS $ "Received mention: " <> showLS messageText

    handleCommand msg $ words txt

    when (txtContainsAnyOf ["bad", "fuck", "dirty", "depraved", "whore", "slut"]) $ do
        liftDiscordWithErrorHandling_ "Bad bot notification"
          . restCall
          $ R.CreateMessage messageChannel ";-;"
        fail ""

    when (T.isInfixOf "pet me" txt) $ do
        liftDiscordWithErrorHandling_ "Good human notification"
          . restCall
          $ R.CreateMessage messageChannel "Good human! *pets*"
        fail ""


    when (T.isInfixOf "lick" txt) $ do
        liftDiscordWithErrorHandling_ "Lick notification"
          . restCall
          $ R.CreateMessage messageChannel "*licks back* :3"
        fail ""

    when (txtContainsAnyOf ["good", "best", "nyaa", "pat", "pet"]) $ do
        liftDiscordWithErrorHandling_ "Good bot notification"
          . restCall
          $ R.CreateMessage messageChannel "Nyaa! :3 :3 :3"
        fail ""

-- For logging when the channel order changes
eventHandler (ChannelUpdate updatedChannel) = void . runMaybeT $ do
    unless (channelGuild updatedChannel == lesbdsmServerId)
      $ fail ""

    discordCache <- liftDiscord readCache
    (_, guildInfo) <- hoistMaybe
                      . DMS.lookup (channelGuild updatedChannel)
                      $ _guilds discordCache

    oldChan <- hoistMaybe
                . headMay
                . filter (on (==) channelId updatedChannel)
                . guildChannels
                $ guildInfo

    when (channelPosition oldChan == channelPosition updatedChannel)
      $ fail ""

    liftDiscordWithErrorHandling_ "Channel moved"
      . restCall
      . R.CreateMessage dynoLogChannel
      $ concat [
          "Channel ",
          mentionChannel (channelId updatedChannel),
          " had position changed: ",
          tshow (channelPosition oldChan),
          " -> ",
          tshow (channelPosition updatedChannel)
        ]


 -- eventHandler e = $(logTM) DebugS $ "Received event: " <> showLS e
eventHandler _ = pure ()

handleCommand :: Message -> [Text] -> MaybeT AppM ()
handleCommand Message{..} [_, "unlock"] = do
    voiceChannel <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
    checkUserCanGrantAccess voiceChannel messageChannel (userId messageAuthor)
    unlockVC messageChannel voiceChannel "Channel is now unlocked - everyone come on in!\nUse `@nyaa-bot lock` to re-enable enforced consent."
    fail ""

handleCommand Message{..} [_, "lock"] = do
    voiceChannel <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
    checkUserCanGrantAccess voiceChannel messageChannel (userId messageAuthor)
    lockVC messageChannel voiceChannel "Channel is now locked - consent will now be required to join.\nUse `@nyaa-bot unlock` to disable this."
    fail ""

handleCommand msg@Message{..} (_:"allow":_) = do
    -- Allow commands can be used to grant access to multiple users at once, because why not?
    voiceChannel <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
    let approver = userId messageAuthor

    if null (userMentions msg)
       then liftDiscordWithErrorHandling_ "Invalid allow command"
            . restCall
            $ R.CreateMessage messageChannel "Silly human - please specify a person to let into the voice channel.\ne.g. `@nyaa-bot allow @Sappho`"
       else processAllowUserRequest voiceChannel messageChannel approver `mapM_` (userMentions msg)
    fail ""

  -- Move users to sleepy time VC
handleCommand msg@Message{..} (_:"sleepy":_) = do
    guildId <- hoistMaybe messageGuild

    if null (userMentions msg)
       then liftDiscordWithErrorHandling_ "Invalid sleepy command"
            . restCall
            $ R.CreateMessage messageChannel "If you're sleepy, why not join the Sleepytime VC?"
       else liftDiscordWithErrorHandling_ "Move users" $ do
              (restCall
                . (\uid -> R.ModifyGuildMember guildId uid (def { R.modifyGuildMemberOptsMoveToChannel = Just sleepyChannel }))
                ) `mapM_` (userMentions msg)
              restCall $ R.CreateMessage messageChannel "Sweet dreams ^_^"
    fail ""

handleCommand Message{..} [_, "ping"] = do
    voiceChannel <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
    currentMembers <- appQuery $ GetChannelMembers voiceChannel

    when (not $ null currentMembers)
      . liftDiscordWithErrorHandling_ "Pinging VC members"
      . restCall
      . R.CreateMessage messageChannel
      . intercalate ", "
      $ map mentionUser currentMembers
    fail ""

handleCommand Message{..} [_, "migrate", dstTxtMention] = do
    dstTxtChanId <- logEither ("Parsing mention: " <> showLS dstTxtMention) $ parseMention dstTxtMention
    dstVC <- hoistMaybe $ DMS.lookup dstTxtChanId voiceChannelMapping
    srcVC <- hoistMaybe $ DMS.lookup messageChannel voiceChannelMapping
    guild <- hoistMaybe messageGuild
    currentMembers <- appQuery $ GetChannelMembers srcVC

    when (not $ null currentMembers)
      . liftDiscordWithErrorHandling_ "Migrate users" $ do
          (restCall
            . (\uid -> R.ModifyGuildMember guild uid (def { R.modifyGuildMemberOptsMoveToChannel = Just dstVC }))
            ) `mapM_` currentMembers
          restCall $ R.CreateMessage messageChannel "See you on the other side ^_^"
    fail ""

handleCommand Message{..} [_, "dance"] = do
    -- Convenience command for Calico so that this doesn't need to be CW'd every time
    liftDiscordWithErrorHandling_ "Posting dance GIF"
    . restCall
    . R.CreateMessageDetailed messageChannel
    $ def {
        R.messageDetailedContent = "CW fast-moving image",
        R.messageDetailedFile = Just ("SPOILER_dance.gif", danceGif)
      }

handleCommand Message{..} [_, "chomp"] = do
    -- Chomp method for SkyeBear
    liftDiscordWithErrorHandling_ "Proceeding to chomp"
    . restCall $ R.CreateMessage messageChannel "Ouch! That hurt! :adhesive_bandage:"

handleCommand Message{..} [_, "hug"] = do
    -- Hug method for SkyeBear
    liftDiscordWithErrorHandling_ "Hug inbound"
    . restCall $ R.CreateMessage messageChannel "*huggles* <3"

handleCommand Message{..} [_, "debug", "reacts", chanId@_, msgId@_] = do
    checkAdminAccess messageChannel (userId messageAuthor)

    msg <- liftDiscordWithErrorHandling "Debug reacts message get"
            . restCall
            $ R.GetChannelMessage (read chanId, read msgId)

    liftDiscordWithErrorHandling_ "Debug reacts message post"
      . restCall
      . R.CreateMessage messageChannel
      $ tshow (messageReactionEmoji <$> T.messageReactions msg)

handleCommand Message{..} [_, "debug", "msg", msgUrl@_] = do
    checkAdminAccess messageChannel (userId messageAuthor)
    (_, chanId, msgId) <- logEither ("Parsing msg link: " <> showLS msgUrl) $ parseMsgLink msgUrl

    msg <- liftDiscordWithErrorHandling "Debug msg get"
            . restCall
            $ R.GetChannelMessage (chanId, msgId)

    liftDiscordWithErrorHandling_ "Debug reacts message post"
      . restCall
      . R.CreateMessage messageChannel
      $ tshow msg

handleCommand Message{..} [_, "deref", msgUrl@_] = do
    (_, chanId, msgId) <- logEither ("Parsing msg link: " <> showLS msgUrl) $ parseMsgLink msgUrl

    msg <- liftDiscordWithErrorHandling "Debug msg get"
            . restCall
            $ R.GetChannelMessage (chanId, msgId)

    let embed = def {
                  createEmbedAuthorName = userName $ T.messageAuthor msg,
                  createEmbedAuthorUrl = fromMaybe "" $ avatarUrl (T.messageAuthor msg),
                  createEmbedDescription = T.messageText msg
                }

    liftDiscordWithErrorHandling_ "Post dereferenced message"
      . restCall
      . R.CreateMessageDetailed messageChannel
      $ def {
          R.messageDetailedContent = "Dereferenced message by " ++ (mentionUser . userId $ T.messageAuthor msg),
          R.messageDetailedEmbed = Just embed,
          R.messageDetailedReference = Just (MessageReference (Just messageId) (Just messageChannel) (messageGuild) True)
        }

handleCommand msg [_, "deref"] = do
    ref <- hoistMaybe $ referencedMessage msg
    msgUrl <- hoistMaybe
              $ headMay
              . filter (T.isPrefixOf "https://discord.com/channels/")
              . words
              . messageText
              $ ref
    handleCommand msg ["", "deref", msgUrl]

-- Generates a link to the role preview site, which helps to identify role colour accessibility issues
handleCommand msg [_, "rolecolours"] = do
    guildId <- hoistMaybe $ messageGuild msg

    roles <- liftDiscordWithErrorHandling "Get roles"
      . restCall
      $ R.GetGuildRoles guildId

    members <- getAllMembers guildId

    -- We only count the highest rank role for each user, since that's what determines their colour
    let roleCount :: Map RoleId Int
        roleCount = DMS.fromListWith (+)
                  . map (\k -> (k, 1))
                  . map highestRanked
                  . filter (\m -> not $ null m)
                  . map memberRoles
                  $ members

        roleRanks :: Map RoleId Integer
        roleRanks = DMS.fromList
                  . map (\r -> (roleId r, rolePos r))
                  $ roles

        highestRanked :: [RoleId] -> RoleId
        highestRanked = head . sortBy (compare `on` (negate . getRankOfRole))

        getCountOfRole roleId = fromMaybe 0 $ DMS.lookup roleId roleCount
        getRankOfRole roleId = fromMaybe 0 $ DMS.lookup roleId roleRanks

    $(logTM) InfoS $ concat [
        "Role counts: ",
        showLS . sortBy (compare `on` (negate . snd)) $ DMS.toList roleCount
      ]

    let roleUrl = rolePreviewUrl
                . filter (\r -> roleColor r /= 0 && getCountOfRole (roleId r) > 1)
                $ roles

    $(logTM) InfoS $ concat [
        "Generated role URL: ",
        showLS roleUrl
      ]

    -- The generated URL is 3 KB, too long to fit in a message, so we put it in a file

    liftDiscordWithErrorHandling_ "Role colour preview link"
      . restCall
      . R.CreateMessageDetailed (messageChannel msg)
      $ def {
          R.messageDetailedContent = "URL for role preview",
          R.messageDetailedFile = Just ("link.txt", encodeUtf8 roleUrl)
        }

handleCommand _ _ = pure ()


-- Unlock a voice channel, allowing anyone to join it
unlockVC 
  :: (MonadDiscord m, MonadFail m, KatipContext m)
  => ChannelId -> ChannelId -> Text -> m ()
unlockVC txtChannel voiceChannel msg = do
    $(logTM) InfoS $ concat [
        "Unlocking VC ",
        showLS voiceChannel
      ]

    -- we reset perms by removing the member-specific grants and setting connect perms on roles
    sendNotif <- modifyChannelPerms voiceChannel
      $ modifyAllowsForToggleableRoles ((.|.) dpConnect)
      . filter ((==) "role" . overwriteType)

    when sendNotif
      . liftDiscordWithErrorHandling_ "Voice empty notification"
      . restCall
      $ R.CreateMessage txtChannel msg

-- Lock a voice channel, so that users need to go through the consent request flow
lockVC
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> ChannelId -> Text -> m ()
lockVC txtChannel voiceChannel msg = do
    $(logTM) InfoS $ concat [
        "Locking VC ",
        showLS voiceChannel
      ]

    -- We lock by unsetting connect perms on roles
    sendNotif <- modifyChannelPerms voiceChannel
      $ modifyAllowsForToggleableRoles ((.&.) (complement dpConnect))

    -- Grant anyone already in the channel access, so that if their connection drops out they can easily reconnect
    currentMembers <- appQuery $ GetChannelMembers voiceChannel
    let newPerms = (\uid -> Overwrite uid "member" dpConnect 0) <$> currentMembers

    _ <- modifyChannelPerms voiceChannel $ (++) newPerms

    when sendNotif
      . liftDiscordWithErrorHandling_ "Voice in use notification"
      . restCall
      $ R.CreateMessage txtChannel msg

-- Business logic for granting or rejecting an attempt to give a user access to a voice channel
processAllowUserRequest
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> ChannelId -> UserId -> UserId -> m ()
processAllowUserRequest voiceChannel textChannel approver candidate = do
    -- Check if the the user is actually authorized to grant access
    checkUserCanGrantAccess voiceChannel textChannel approver

    -- Check if the user tried to grant access to the bot
    botUser <- userId . _currentUser <$> liftDiscord readCache

    when (candidate == botUser) $ do
      liftDiscordWithErrorHandling_ "User tried to grant access to bot"
        . restCall
        $ R.CreateMessage textChannel
        $ concat [
            "Silly human - I grant access to you, not the other way around. :3\n",
            "You're supposed to react on the message someone posts to ask for access, not mine."
          ]
      fail ""

    $(logTM) InfoS $ concat [
            "User ",
            showLS approver,
            " approved user ",
            showLS candidate,
            " for access to channel ",
            showLS voiceChannel
        ]

    -- Update perms
    -- Note that because we only grant access by adding user-specific perms, we can later reset perms by just dropping all non-role perms
    sendNotif <- modifyChannelPerms voiceChannel $ (:) (Overwrite candidate "member" dpConnect 0)

    -- TODO: ideally we'd use the reply feature, but CreateMessage constructs the JSON blob for us so we'd need to create a new API for doing so
    -- See: https://hackage.haskell.org/package/discord-haskell-1.8.3/docs/src/Discord.Internal.Rest.Channel.html#local-6989586621679139873
    when sendNotif
      . liftDiscordWithErrorHandling_ "User granted access"
      . restCall
      $ R.CreateMessage textChannel
      $ concat [
          ":white_check_mark: ",
          mentionUser candidate,
          " you have been granted access by ",
          mentionUser approver,

          -- Easter egg
          fromMaybe " - come on in ^_^"
          $ lookup candidate [
              (kawaUID,  ".\nWelcome back, Hime-sama!"),
              (jennyUID, ".\nI sense a great disturbance in the force, as if a million subs cried out and were suddenly denied."),
              (blakeUID, ".\nWe are now at BRATCON 1."),
              (skyeUID,  ".\nChomp! :bear:"),
              (myceUID,  ".\n*gives the good bun lots of pets* :rabbit:")
            ]
        ]

-- Helper function for modifying channel permissions
-- Returns true if this actually changed the perms, or false if it was a no-op.
modifyChannelPerms
  :: (MonadDiscord m, MonadFail m, KatipContext m)
  => ChannelId -> ([Overwrite] -> [Overwrite]) -> m Bool
modifyChannelPerms cid permFunc = do
  channelInfo <- liftDiscordWithErrorHandling ("Fetching channel info for " <> showLS cid)
    . restCall
    $ R.GetChannel cid

  let newPerms = permFunc $ channelPermissions channelInfo

  liftDiscordWithErrorHandling_ ("Updating channel perms for " <> showLS cid)
    . restCall
    . R.ModifyChannel cid
    $ def { R.modifyChannelPermissionOverwrites = Just newPerms }

  return $ (channelPermissions channelInfo) /= newPerms

-- Helper function for getting the number of users in a channel
channelMemberCount ::(MonadReader AppContext m, MonadIO m) => Maybe ChannelId -> MaybeT m Int
channelMemberCount = maybe (pure 0) (map length . appQuery . GetChannelMembers)

-- Lifts the specified mutation to allows for toggleable roles to the level of Overwrites, for use with modifyChannelPerms
modifyAllowsForToggleableRoles :: (Integer -> Integer) -> ([Overwrite] -> [Overwrite])
modifyAllowsForToggleableRoles allowMod = res where
  res = mapIf (flip elem rolesToToggle . overwriteId)
              (\o -> o { overwriteAllow = allowMod (overwriteAllow o) })

  -- Applies a mapping function (f) only to elements satisfying the predicate (p)
  mapIf p f = map f' where
    f' x | p x       = f x
         | otherwise = x

-- Performs permission checking, invoking fail if the user does not have access
checkUserCanGrantAccess
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> ChannelId -> UserId -> m ()
checkUserCanGrantAccess voiceChannel textChannel approver = do
    approverChannel <- appQuery $ GetUserChannel approver

    when (Just voiceChannel /= approverChannel) $ do
      liftDiscordWithErrorHandling_ "Approver failed authorization"
        . restCall
        $ R.CreateMessage textChannel
        $ concat [
            ":warning: ",
            mentionUser approver,
            " you cannot manage access to the channel as you are not in it."
          ]
      fail ""

checkAdminAccess
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> UserId -> m ()
checkAdminAccess textChannel uid = unless (uid == reneeUID) $ do
  liftDiscordWithErrorHandling_ "Failed admin auth check"
    . restCall
    $ R.CreateMessage textChannel
    $ "You don't have enough badges to tame me, human! >:3"
  fail ""

isVCLocked
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> m Bool
isVCLocked cid = do
  channelPerms <- map channelPermissions
                  . liftDiscordWithErrorHandling "Get channel"
                  . restCall
                  $ R.GetChannel cid
  let testRole = head rolesToToggle
      testPerms
        = fromMaybe 0
        . headMay
        . map overwriteAllow
        . filter ((==) testRole . overwriteId)
        . filter ((==) "role" . overwriteType)
        $ channelPerms

  return $ testPerms .&. dpConnect == 0

getAllMembers
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => GuildId -> m [GuildMember]
getAllMembers guildId =  do
  let -- loop :: R.GuildMembersTiming -> _ [[GuildMember]]
      loop page = do
        res :: [GuildMember]
            <- liftDiscordWithErrorHandling "Get all members"
               . restCall
               $ R.ListGuildMembers guildId page

        if null res
           then return (res : [])
           else do
             let nextPage = R.GuildMembersTiming
                              (Just 1000)
                              (Just . userId . memberUser $ last res)

             res' <- loop nextPage
             return $ res : res'

  map join
    . loop
    $ R.GuildMembersTiming (Just 1000) Nothing


userMentions :: Message -> [UserId]
userMentions
  = map userId
  . filter (not . userIsBot)
  . messageMentions

hoistMaybe :: Monad m => Maybe a -> MaybeT m a
hoistMaybe = MaybeT . return

-- Extracts the right value, or logs an error and fails if left.
logEither :: (MonadFail m, KatipContext m, Show l)
          => LogStr -> Either l r -> m r
logEither msg (Left e) = $(logTM) ErrorS (msg <> ": " <> showLS e) >> fail ""
logEither _ (Right x) = pure x

-- Variant of liftDiscord that handles Either results via logEither
-- While fairly concise, this is a lot easier to work with written as a separate function rather than inlined
liftDiscordWithErrorHandling
  :: (MonadDiscord m, MonadFail m, KatipContext m, Show l)
  => LogStr -> DiscordHandler (Either l r) -> m r
liftDiscordWithErrorHandling msg f = logEither msg =<< liftDiscord f

liftDiscordWithErrorHandling_
  :: (MonadDiscord m, MonadFail m, KatipContext m, Show l)
  => LogStr -> DiscordHandler (Either l r) -> m ()
liftDiscordWithErrorHandling_ msg f = void $ liftDiscordWithErrorHandling msg f
