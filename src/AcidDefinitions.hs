{-# LANGUAGE StrictData, TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module AcidDefinitions where

import BasicPrelude
import Control.Monad.Reader.Class (ask)
import Control.Monad.State.Class (modify)
import Data.Acid (Query, Update, makeAcidic)
import Data.Default.Class (Default)
import Data.SafeCopy (deriveSafeCopy, base)
import Discord.Types (UserId, ChannelId, Snowflake)


-- Using an association list cause its simple and the list size means performance impact should be negligible
newtype AppDB = AppDB [(UserId, ChannelId)]
  deriving (Eq, Show, Typeable, Default)

unAppDB :: AppDB -> [(UserId, ChannelId)]
unAppDB (AppDB x) = x

$(deriveSafeCopy 0 'base ''AppDB)
$(deriveSafeCopy 0 'base ''Snowflake)


-- Operations
getUserChannel :: UserId -> Query AppDB (Maybe ChannelId)
getUserChannel uid = (lookup uid . unAppDB) <$> ask

getChannelMembers :: ChannelId -> Query AppDB [UserId]
getChannelMembers cid = (map fst . filter ((==) cid . snd) . unAppDB) <$> ask

updateUserChannel :: UserId -> ChannelId -> Update AppDB ()
updateUserChannel uid cid = modify $ (AppDB . prepend . remove . unAppDB) where
  prepend = (:) (uid, cid)
  remove = filter ((/=) uid . fst)

removeUserChannel :: UserId -> Update AppDB ()
removeUserChannel uid = modify $ (AppDB . remove . unAppDB) where
  remove = filter ((/=) uid . fst)

$(makeAcidic ''AppDB ['getUserChannel, 'getChannelMembers, 'updateUserChannel, 'removeUserChannel])
