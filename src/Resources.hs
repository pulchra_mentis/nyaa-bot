module Resources where

import BasicPrelude
import Data.FileEmbed

danceGif :: ByteString
danceGif = $(embedFile "resources/dance.gif")

