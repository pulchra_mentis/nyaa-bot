#!/bin/bash
set -exuo pipefail

STACK_VERSION="2.7.3"
STACK_SHA256="47620a41b06e0b45371154ad4a176d3ff92a50e1f0feac64439011b9240a55fa"

# Apt dependencies
apt-get update && apt-get install -y zlib1g-dev netbase curl build-essential libgmp-dev openssh-client

# Install stack from stackage, because the version that ships with Ubuntu isn't guarranteed to work with Stack repos
curl -LsSf "https://github.com/commercialhaskell/stack/releases/download/v${STACK_VERSION}/stack-${STACK_VERSION}-linux-x86_64-bin" > /usr/local/bin/stack
chmod +x /usr/local/bin/stack
echo "${STACK_SHA256}  /usr/local/bin/stack" | sha256sum -c

