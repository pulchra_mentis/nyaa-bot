#!/bin/bash
set -euxo pipefail

cd deployment

# Populate SSH key
mkdir -p ~/.ssh
(umask 066 ; echo "$PRIVATE_SSH_KEY" | base64 -d > ~/.ssh/id_rsa)
echo "$VPS ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGetKgb3gJClbfHn7UAG2FeSfFqB5iCUij31p1rqIyi9" > ~/.ssh/known_hosts

ssh "$VPS" id
WORKDIR="$(ssh "$VPS" mktemp -d)"
# VPS - hostname of vps

scp -C ./nyaa-bot.service "$(stack path --local-install-root)/bin/nyaa-bot" "$VPS:$WORKDIR"

ssh $VPS <<EOF
    set -x
    mv "$WORKDIR/nyaa-bot.service" /etc/systemd/system/
    systemctl daemon-reload

    mv "$WORKDIR/nyaa-bot" /opt/nyaa-bot/
    systemctl restart nyaa-bot
    rmdir "$WORKDIR"
EOF

